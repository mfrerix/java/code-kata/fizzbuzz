# FizzBuzz

Dies ist meine Lösung der klassischen FizzBuzz Aufgabe

Die Aufgabe
-
Schreiben Sie ein Programm, das die Zahlen von 1 bis 100 ausgibt. 
Bei jeder Zahl, die durch x teilbar ist, soll "fizz" ausgegeben werden und bei 
jeder Zahl, die durch y teilbar ist, soll "buzz" ausgegeben werden. 
Wenn die Zahl sowohl durch 7 als auch durch 5 teilbar ist, soll "fizzbuzz" 
ausgegeben werden

Erweiterung:
-
Zusätzlich soll "fizz" bzw. "buzz" ausgegeben werden wenn die Zahl x bzw. y 
enthält. "fizzbuzz" soll ausgegeben werden die zahl x und y enthält 