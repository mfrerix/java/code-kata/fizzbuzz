package de.mfrerix.fizzbuzz.rule;

import org.junit.Test;

import static org.junit.Assert.*;

public class DivisorRuleTest {

    @Test
    public void numberIsDivisibleByDivisorReturnTrue() {
        Rule rule = new DivisorRule(3, null);

        assertTrue(rule.isRuleMet(6));

    }

    @Test
    public void numberIsNotDivisibleByDivisorReturnFalse() {
        Rule rule = new DivisorRule(3, null);
        assertFalse(rule.isRuleMet(5));
    }

    @Test
    public void RuleOutputIsGivenOutput() {
        Rule rule = new DivisorRule(3, "Output");
        assertEquals("Output", rule.getAlternativeOutput());
    }
}
