package de.mfrerix.fizzbuzz.rule;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class RuleProcessorTest {

    @Test
    public void processingRuleIgnoreOtherRuleAfterFirstMatch() {
        Rule matchingRule1 = createMatchingRule();
        Rule matchingRule2 = createMatchingRule();
        Rule nonMatchingRule1 = CreateNonMatchingRule();
        Rule nonMatchingRule2 = CreateNonMatchingRule();
        Rule nonMatchingRule3 = CreateNonMatchingRule();
        List<Rule> rules = Arrays.asList(nonMatchingRule1,nonMatchingRule2, matchingRule1, nonMatchingRule3, matchingRule2);
        RuleProcessor ruleProcessor = new RuleProcessor();
        ruleProcessor.processRules(rules, 0);

        Assert.assertEquals("true", nonMatchingRule1.getAlternativeOutput().get());
        Assert.assertEquals("true", nonMatchingRule2.getAlternativeOutput().get());
        Assert.assertEquals("true", matchingRule1.getAlternativeOutput().get());
        Assert.assertEquals("false", nonMatchingRule3.getAlternativeOutput().get());
        Assert.assertEquals("false", matchingRule2.getAlternativeOutput().get());

    }

    private Rule createMatchingRule() {
        return new Rule() {

            private boolean wasCalled = false;
            @Override
            public boolean isRuleMet(int number) {
                wasCalled = true;
                return true;
            }

            @Override
            public Optional<String> getAlternativeOutput() {
                return Optional.of(Boolean.toString(wasCalled));
            }
        };
    }

    private Rule CreateNonMatchingRule() {
        return new Rule() {

            private boolean wasCalled = false;

            @Override
            public boolean isRuleMet(int number) {
                wasCalled = true;
                return false;
            }

            @Override
            public Optional<String> getAlternativeOutput() {
                return Optional.of(Boolean.toString(wasCalled));
            }
        };
    }
}
