package de.mfrerix.fizzbuzz.rule;

import org.junit.Test;

import java.util.Arrays;
import java.util.Optional;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class NumberRuleTest {

    @Test
    public void ifOtherRuleMetThisRuleDontMet() {
        Rule alwaysTrueRule = new Rule() {
            @Override
            public boolean isRuleMet(int number) {
                return true;
            }

            @Override
            public Optional<String> getAlternativeOutput() {
                return Optional.empty();
            }
        };
        Rule rule = new NumberRule(Arrays.asList(alwaysTrueRule));
        assertFalse(rule.isRuleMet(3));
    }

    @Test
    public void ifNoOtherRuleMetThisRuleMet() {
        Rule alwaysFalseRule = new Rule() {
            @Override
            public boolean isRuleMet(int number) {
                return false;
            }

            @Override
            public Optional<String> getAlternativeOutput() {
                return Optional.empty();
            }
        };
        Rule rule = new NumberRule(Arrays.asList(alwaysFalseRule));
        assertTrue(rule.isRuleMet(3));
    }


}
