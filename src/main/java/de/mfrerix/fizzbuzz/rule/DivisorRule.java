package de.mfrerix.fizzbuzz.rule;

import java.util.Optional;

public class DivisorRule implements Rule {

    private int divisor;
    private String output;

    public DivisorRule(int divisor, String output) {
        this.divisor = divisor;
        this.output = output;
    }

    public boolean isRuleMet(int number) {
        return number % divisor == 0;
    }

    public Optional<String> getAlternativeOutput() {
        return Optional.of(output);
    }
}
