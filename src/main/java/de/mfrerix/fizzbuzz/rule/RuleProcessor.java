package de.mfrerix.fizzbuzz.rule;

import java.util.List;

public class RuleProcessor {



    public void processRules(List<Rule> rules, int number) {
        Rule matchingRule = getMatchingRule(rules, number);
        System.out.println(matchingRule.getAlternativeOutput().orElse(String.valueOf(number)));
    }

    private Rule getMatchingRule(List<Rule> rules, int i) {
        return rules.stream()
                .filter(rule -> rule.isRuleMet(i))
                .findFirst()
                .get();
    }


}
