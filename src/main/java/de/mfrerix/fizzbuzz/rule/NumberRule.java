package de.mfrerix.fizzbuzz.rule;

import java.util.List;
import java.util.Optional;

public class NumberRule implements Rule {

    private List<Rule> rules;

    public NumberRule(List<Rule> rules) {
        this.rules = rules;

    }

    @Override
    public boolean isRuleMet(int number) {
        return rules.stream().noneMatch(rule -> rule.isRuleMet(number));
    }

    @Override
    public Optional<String> getAlternativeOutput() {
       return Optional.empty();
    }
}
