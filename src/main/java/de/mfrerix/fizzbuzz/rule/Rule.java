package de.mfrerix.fizzbuzz.rule;

import java.util.Optional;

public interface Rule {

    boolean isRuleMet(int number);

    Optional<String> getAlternativeOutput();

}
