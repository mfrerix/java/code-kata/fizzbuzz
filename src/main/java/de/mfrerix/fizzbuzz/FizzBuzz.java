package de.mfrerix.fizzbuzz;

import de.mfrerix.fizzbuzz.rule.DivisorRule;
import de.mfrerix.fizzbuzz.rule.NumberRule;
import de.mfrerix.fizzbuzz.rule.Rule;
import de.mfrerix.fizzbuzz.rule.RuleProcessor;

import java.util.Arrays;
import java.util.List;

public class FizzBuzz {

    private static final int START = 1;
    private static final int END = 100;

    public static void main(String[] args) {
        FizzBuzz fizzBuzz = new FizzBuzz();
        fizzBuzz.playFizzBuzz(Integer.parseInt(args[0]), Integer.parseInt(args[1]));

    }

    private void playFizzBuzz(int x, int y) {
        Rule xRule = new DivisorRule(x, "Fizz");
        Rule yRule = new DivisorRule(y, "Buzz");
        Rule xyRule = new DivisorRule(x * y, "FizzBuzz");
        RuleProcessor ruleProcessor = new RuleProcessor();
        List<Rule> rules = Arrays.asList(xyRule, xRule, yRule, new NumberRule((Arrays.asList(xyRule, xRule, yRule))));

        int counter = START;
        while (counter < END) {
            ruleProcessor.processRules(rules, counter);
            counter++;
        }
    }
}
